package io.pillopl.newmodel.catalogue

import spock.lang.Specification

class BookTitleAuthorISBNTest extends Specification {


    def "title should not be null"() {
        when:
            new Title(null)
        then:
            thrown(NullPointerException)
    }

    def "title should not be longer than 100"() {
        when:
            new Title("Very Long Title Very Long Title Very Long Title Very Long Title Very Long Title Very Long Title Very Very")
        then:
            thrown(IllegalArgumentException)
    }


    def "author should not be null"() {
        when:
            new Author(null)
        then:
            thrown(NullPointerException)
    }


    def "author should not be longer than 60"() {
        when:
            new Author("Very Long Name, Perhaps one taken after mother and another after father. We don't support it.")
        then:
            thrown(IllegalArgumentException)
    }

    def "isbn should be correct"() {
        when:
            ISBN isbn = new ISBN("123412341X")
        then:
            isbn.isbn == "123412341X"
    }

    def "isbn should be trimmed"() {
        when:
            ISBN isbn = new ISBN("  1234123414  ")
        then:
            isbn.isbn == "1234123414"
    }

    def "wrong isbn should not be accepted"() {
        when:
            new ISBN("not isbn")
        then:
            thrown(IllegalArgumentException)
    }
}


