package io.pillopl.newmodel.lending.domain.patron


import io.pillopl.newmodel.lending.domain.book.AvailableBook
import io.pillopl.newmodel.lending.domain.patron.events.BookCollected
import spock.lang.Specification

import java.time.Period

import static Fixtures.aRegularPatron
import static Fixtures.circulatingBook

class CollectingScenarios extends Specification {

    def 'can collect an existing hold'() {
        given:
            Patron patron = aRegularPatron()
        and:
            AvailableBook book = circulatingBook()
        and:
            patron.placeOnHold(book)
        when:
            Optional<BookCollected> event = patron.collect(book.bookId)
        then:
            event.isPresent()
    }

    def 'cannot collect when book is not on hold'() {
        given:
            Patron patron = aRegularPatron()
        and:
            AvailableBook book = circulatingBook()
        when:
            Optional<BookCollected> event = patron.collect(book.bookId)
        then:
            !event.isPresent()
    }

    def 'collect duration can be less then or 60 days'() {
        when:
            new CollectDuration(Period.ofDays(days))
        then:
            thrown(IllegalArgumentException)
        where:
            days << (61..100)
    }

    def 'collect duration cannot be more than 60 days'() {
        when:
            new CollectDuration(Period.ofDays(days))
        then:
            noExceptionThrown()
        where:
            days << (1..60)
    }
}
