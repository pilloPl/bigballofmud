package io.pillopl.newmodel.lending.domain.patron

import io.pillopl.newmodel.lending.domain.patron.events.PlacedOnHold
import spock.lang.Specification

import static io.pillopl.newmodel.lending.domain.patron.Fixtures.*

class PlacingOnHoldScenarios extends Specification {


    def 'can place on hold circulating book if patron is regular'() {
        given:
            Patron patron = aRegularPatron()
        when:
            Optional<PlacedOnHold> event = patron.placeOnHold(circulatingBook())
        then:
            event.isPresent()
    }

    def 'cannot place on hold restricted book if patron is regular'() {
        given:
            Patron patron = aRegularPatron()
        when:
            Optional<PlacedOnHold> event = patron.placeOnHold(restrictedBook())
        then:
            !event.isPresent()
    }

    def 'can place on hold restricted book if patron is researcher'() {
        given:
            Patron patron = aResearcherPatron()
        when:
            Optional<PlacedOnHold> event = patron.placeOnHold(restrictedBook())
        then:
            event.isPresent()
    }

    def 'can place on hold up to 5 books'() {
        given:
            Patron patron = aResearcherPatron()
        and:
            5.times { patron.placeOnHold(circulatingBook())}
        when:
            Optional<PlacedOnHold> event = patron.placeOnHold(circulatingBook())
        then:
            !event.isPresent()
    }

    def 'cannot place on hold when there are 2 overdue collected books'() {
        given:
            Patron patron = aResearcherPatronWithTwoOverdueBooks()
        when:
            Optional<PlacedOnHold> event = patron.placeOnHold(circulatingBook())
        then:
            !event.isPresent()
    }

    def 'cannot place open-ended hold if patron is regular'() {
        given:
            Patron patron = aRegularPatron()
        when:
            Optional<PlacedOnHold> event = patron.placeOnHold(circulatingBook(), new OpenEndedHoldDuration())
        then:
            !event.isPresent()
    }

    def 'can place open-ended hold if patron is researcher'() {
        given:
            Patron patron = aResearcherPatron()
        when:
            Optional<PlacedOnHold> event = patron.placeOnHold(circulatingBook(), new OpenEndedHoldDuration())
        then:
            event.isPresent()
    }
}
