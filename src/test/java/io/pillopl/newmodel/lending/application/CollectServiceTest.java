package io.pillopl.newmodel.lending.application;

import io.pillopl.newmodel.DomainEvents;
import io.pillopl.newmodel.lending.domain.book.AvailableBook;
import io.pillopl.newmodel.lending.domain.patron.*;
import io.pillopl.newmodel.lending.domain.patron.events.BookCollected;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class CollectServiceTest {

    PatronRepository patronRepository = new io.pillopl.newmodel.lending.infrastructure.InMemoryPatronRepository();
    DomainEvents events = mock(DomainEvents.class);

    CollectService collectService = new CollectService(patronRepository, events);

    @Test
    public void shouldPublishAnEventWhenOperationWasSuccessful() {
        //given
        Patron patron = persistedRegularPatron();
        //and
        AvailableBook book = Fixtures.circulatingBook();
        //and
        patron.placeOnHold(book);

        //when
        collectService.placeOnHold(new CollectCommand(CollectDuration.forOneMonth(), book.getBookId(), patron.getPatronId()));

        //then
        verify(events).publish(isA(BookCollected.class));

    }

    @Test
    public void shouldNotPublishAnEventWhenOperationWasNotSuccessful() {
        //given
        Patron patron = persistedRegularPatron();
        //and
        AvailableBook book = Fixtures.circulatingBook();

        //when
        collectService.placeOnHold(new CollectCommand(CollectDuration.forOneMonth(), book.getBookId(), patron.getPatronId()));

        //then
        verifyZeroInteractions(events);
    }

    Patron persistedRegularPatron() {
        Patron patron = Fixtures.aRegularPatron();
        patronRepository.save(patron);
        return patron;
    }

}