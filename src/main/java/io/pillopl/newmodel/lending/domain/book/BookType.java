package io.pillopl.newmodel.lending.domain.book;

public enum BookType {
    Restricted, Circulating
}
