package io.pillopl.newmodel.lending.domain.patron;

public enum PatronType {
    Regular, Researcher
}


