package io.pillopl.newmodel.lending.application;

import io.pillopl.newmodel.DomainEvents;
import io.pillopl.newmodel.lending.domain.patron.Patron;
import io.pillopl.newmodel.lending.domain.patron.PatronRepository;
import io.pillopl.newmodel.lending.domain.patron.events.BookCollected;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
public class CollectService {

    private final PatronRepository patronRepository;
    private final DomainEvents domainEvents;

    Result placeOnHold(CollectCommand command) {
        Patron patron = findPatron(command);
        Optional<BookCollected> event = patron.collect(command.getBookId());
        event.ifPresent(domainEvents::publish);
        patronRepository.save(patron);
        if (event.isPresent()) {
            return Result.Allowance;
        } else {
            return Result.Rejection;
        }
    }

    private Patron findPatron(CollectCommand command) {
        return patronRepository.findById(command.getPatronId()).orElseThrow(IllegalArgumentException::new);
    }

}

