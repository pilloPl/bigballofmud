package io.pillopl.newmodel.lending.application;

public enum Result {

    Allowance, Rejection
}
